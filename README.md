# TAXUD Management Scripts Repository

This is a public repository used for TAXUD management environment.

The goal of this documentation is to explain different automation components for TAXUD.

## Start working on the project
[CONTRIBUTING]: https://code.europa.eu/digit-c4/net/dg-service/-/blob/txd_initial/CONTRIBUTING.md?ref_type=heads

To start working on this project, refer to the CONTRIBUTING page [CONTRIBUTING].

## Launch the scripts via CLI
Make sure the correct hosts is in the *inventory.txt*

ansible-playbook -i inventory.txt *playbook name*

## Launch the scripts via AWX 

TBD
